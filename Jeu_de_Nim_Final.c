#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
// VARIABLES GLOBALES
int faconJeu, nb_enlever, joueur, nb_allumettes;
// DEFINITION DES FONCTIONS
int changement_joueur(int joueur)
{
    if (joueur == 1)
    {
        joueur = 2;
    }
    else
    {
        joueur = 1;
    }
    return joueur;
}
int allumettesARetirer_joueur(int nb_allumettes)
{
    do
    {
        printf("Combien d'allumettes voulez-vous enlever?\n (nombre entre 0 et 3) ou vérifiez le nombre restant d'allumettes ");
        scanf("%d", &nb_enlever);
        printf("\n");
    } while ((nb_enlever < 1 || nb_enlever > 3) && (nb_enlever <= nb_allumettes));
    return nb_enlever;
}
int allumettesARetirer_random(int nb_allumettes)
{
    if (nb_allumettes == 1)
    {
        nb_enlever = 1;
    }
    else if (nb_allumettes == 2)
    {
        nb_enlever = rand() % 2 + 1;
    }
    else
    {

        nb_enlever = rand() % 3 + 1;
    } // nombre aléatoire entre 1 et 3
    return nb_enlever;
}
int allumettesARetirer_intelligent(int nb_allumettes)
{
    nb_enlever = (nb_allumettes - 1) % 4; // il faut laisser un nombre n congru à 4 modulo 1
    if (nb_enlever == 0)
    {
        nb_enlever = allumettesARetirer_random(nb_allumettes);
    }
    return nb_enlever;
}

int jeuHumain_et_Bot(){
    if (joueur == 1){
        nb_allumettes -= allumettesARetirer_joueur(nb_allumettes);
    }
    else{
        if (faconJeu == 2){
            nb_enlever = allumettesARetirer_random(nb_allumettes);
            printf("Le bot a retiré %d allumettes \n", nb_enlever);
            nb_allumettes -= nb_enlever;
        }
        else{
            nb_enlever = allumettesARetirer_intelligent(nb_allumettes);
            printf("Le bot a retiré %d allumettes \n", nb_enlever);
            nb_allumettes -= nb_enlever;
        }
    }
    return nb_allumettes;
}

int jeuBot_Bot()
{
    if (joueur == 1)
    {
        nb_enlever = allumettesARetirer_random(nb_allumettes);
        17 LAHYA Nadia &DIFI Camélia
                TD -
            2 TP - D printf("Le bot débile a retiré %d allumettes \n", nb_enlever);
        nb_allumettes -= nb_enlever;
    }
    else
    {
        nb_enlever = allumettesARetirer_intelligent(nb_allumettes);
        printf("Le bot intelligent a retiré %d allumettes \n", nb_enlever);
        nb_allumettes -= nb_enlever;
    }
    return nb_allumettes;
}
// PROGRAMME PRINCIPAL
int main()
{
    printf("\n");
    printf("BIENVENUS DANS LE JEU DE NIM!\n");
    printf("\n");
    printf("Comment voulez-vous jouer? \n 1: Joueur-Joueur \n 2: Joueur-Bot <<débile>>\n 3: Joueur-Bot (Strategie gagnante) \n 4: Bot<<débile>>-Bot(Strategie gagnante) \n");
    scanf("%d", &faconJeu);
    printf("\n");
    printf("RAPPEL: le joueur qui retire la dernière allumette à perdu\n");
    printf("\n");
    printf("Combien d'allumettes voulez-vous mettre en jeu? ");
    scanf("%d", &nb_allumettes);
    printf("Vous avez %d allumettes mises en jeu.", nb_allumettes);
    printf("\n");
    printf("\n");
    joueur = 1;

    while (nb_allumettes > 0 || nb_allumettes >= nb_enlever)
    {
        printf("Joueur %d : \n", joueur);
        if (faconJeu == 1)
        {
            nb_allumettes -= allumettesARetirer_joueur(nb_allumettes);
        }
        if (faconJeu == 2 || faconJeu == 3)
        {
            printf("Vous êtes le joueur 1 \nLe bot est le joueur 2\n");
            printf("\n");
            jeuHumain_et_Bot();
            printf("\n");
        }
        if (faconJeu == 4)
        {
            printf("Le bot débile est le joueur 1 \nLe bot inteligent est le joueur 2\n");
            printf("\n");
            jeuBot_Bot();
            printf("\n");
        }
        if (nb_allumettes > 0)
        {
            printf("Desormais, il reste %d allumettes en jeu \n", nb_allumettes);
            printf("-----------------------------------------------------------------\n");
        }
        joueur = changement_joueur(joueur);
    }
    printf("FELICITATIONS! Le joueur %d à GAGNE la partie. \n", joueur);
    joueur = changement_joueur(joueur);
    printf("Le joueur %d à PERDU en retirant la dernière allumette. \n", joueur);
    return 0;
}